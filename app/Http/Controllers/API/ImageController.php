<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Image;

class ImageController extends Controller
{
    public function saveImage(Request $request){

        $validator = Validator::make($request->all(), [
            'image' => ['required','mimes:jpeg,jpg']
        ]);

        if ($validator->fails()) {

            return  response()->json([
                "code"=>"422",
                "error"=>true,
                "message"=>'در هنگام ثبت خطایی رخ داد',
                "data"=>$validator->messages()
            ],422);
        }

        $path = $request->file('image')->store('image');

        $img=new Image;
        $img->path=$path;
        $img->save();

       return response()->json([
        "code"=>"200",
        "error"=>false,
        "message"=>'ثبت با موفقیت انجام شد',
        "data"=>['id'=>$img->id]
        ]);


    }

    public function showImage(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => ['required','exists:App\Models\Image,id'],
            'width' => ['required','integer'],
            'height' => ['required','integer'],
            'quality' => ['required','integer'],
            'padding' => ['required','integer'],
        ]);
        if ($validator->fails()) {

            return  response()->json([
                "code"=>"422",
                "error"=>true,
                "message"=>'در هنگام ثبت خطایی رخ داد',
                "data"=>$validator->messages()
            ],422);
        }

        $img=Image::find($request->id);
        $mypath=storage_path('app/'.$img->path);
        $image = new \Imagick($mypath);
        $image->setImageBackgroundColor('white');
        $image->resizeImage($request->width, $request->height,\Imagick::FILTER_CATROM,1-($request->quality/100));
        return response($image->getImageBlob(), 200)
        ->header('Content-Type', 'image/jpg');
     
    }
}
